EESchema Schematic File Version 4
LIBS:naisuRingBoxu-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED:WS2812B D?
U 1 1 5E223E15
P 5550 3600
AR Path="/5E223E15" Ref="D?"  Part="1" 
AR Path="/5E223B45/5E223E15" Ref="D?"  Part="1" 
F 0 "D?" H 5894 3646 50  0000 L CNN
F 1 "WS2812B" H 5894 3555 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 5600 3300 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 5650 3225 50  0001 L TNN
	1    5550 3600
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D?
U 1 1 5E223E1C
P 6150 3600
AR Path="/5E223E1C" Ref="D?"  Part="1" 
AR Path="/5E223B45/5E223E1C" Ref="D?"  Part="1" 
F 0 "D?" H 6494 3646 50  0000 L CNN
F 1 "WS2812B" H 6494 3555 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 6200 3300 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 6250 3225 50  0001 L TNN
	1    6150 3600
	1    0    0    -1  
$EndComp
Text Notes 5800 3150 0    50   ~ 0
top
$Comp
L power:GND #PWR?
U 1 1 5E223E24
P 6450 3900
AR Path="/5E223E24" Ref="#PWR?"  Part="1" 
AR Path="/5E223B45/5E223E24" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6450 3650 50  0001 C CNN
F 1 "GND" H 6455 3727 50  0000 C CNN
F 2 "" H 6450 3900 50  0001 C CNN
F 3 "" H 6450 3900 50  0001 C CNN
	1    6450 3900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5E223E2A
P 5250 3300
AR Path="/5E223E2A" Ref="#PWR?"  Part="1" 
AR Path="/5E223B45/5E223E2A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5250 3150 50  0001 C CNN
F 1 "+5V" H 5265 3473 50  0000 C CNN
F 2 "" H 5250 3300 50  0001 C CNN
F 3 "" H 5250 3300 50  0001 C CNN
	1    5250 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3300 5550 3300
Connection ~ 5550 3300
Wire Wire Line
	5550 3300 6150 3300
Wire Wire Line
	5550 3900 6150 3900
Connection ~ 6150 3900
Wire Wire Line
	6150 3900 6450 3900
Text GLabel 5250 3600 0    50   Input ~ 0
topLED
$EndSCHEMATC
